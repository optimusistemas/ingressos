/**
 * Created by diogo on 04/04/2016.
 */



'use strict';


var app = angular.module('concertsApp', ['ngAnimate', 'ui.bootstrap','firebase']);

// Re-usable factory that generates the $firebaseAuth instance
app.factory("Auth", function($firebaseAuth) {
    var ref = new Firebase("https://ingressos-optimus.firebaseio.com");
    return $firebaseAuth(ref);
});

    app.controller('concertController',
        function($scope, $firebaseObject,$uibModal,$log,Auth) {
            var ref = new Firebase("https://ingressos-optimus.firebaseio.com/");

            var concerts= $firebaseObject(ref.child("concerts"));
            this.concerts = concerts;

        $scope.showTickets = function (item) {
        item.open = !item.open;
        $scope.titleTicketBtn = item.open ? 'Ocultar Ingressos':'Ver Ingressos';
      };

            Auth.$onAuth(function(authData) {
                $scope.authData = authData;
            });
            $scope.Logout = function (e) {
                e.preventDefault();
                //Logout in Firebase
                Auth.$unauth();
            }
      
            $scope.sell=null;
      
            $scope.animationsEnabled = true;

      $scope.open = function (concert,ticket) {
        $scope.sell=concert;

        var modalInstance = $uibModal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'myModalContent.html',
          controller: 'ModalInstanceCtrl',
          resolve: {
            sell: function () {
              $scope.sell.ticket = [];
              $scope.sell.ticket.push(ticket);
              return $scope.sell;
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          $scope.selected = selectedItem;
        }, function () {
          $log.info('Modal dismissed at: ' + new Date());
        });
      };

      $scope.toggleAnimation = function () {
        $scope.animationsEnabled = !$scope.animationsEnabled;
      };



    });

// Please note that $uibModalInstance represents a modal window (instance) dependency.
// It is not the same as the $uibModal service used above.

angular.module('concertsApp').controller('ModalInstanceCtrl',
    function ($scope,$uibModalInstance, sell, Auth) {


        // Listens for changes in authentication state
        Auth.$onAuth(function(authData) {
            $scope.authData = authData;
        });

        $scope.alerts = [];
        $scope.loading = false;


        // Logs in a user with email and password
        $scope.SignIn = function (e) {

            $scope.loading = true;

            Auth.$authWithPassword({
                email: $scope.user.email,
                password: $scope.user.password

            }).catch(function(error) {
                $scope.loading = false;
                if(error) {
                    switch (error.code) {
                        case "INVALID_EMAIL":
                            $scope.alerts.push({
                                type: 'warning',
                                msg: 'E-mail não cadastrado'
                            });
                            break;
                        case "INVALID_PASSWORD":
                            $scope.alerts.push({
                                type: 'danger',
                                msg: 'Senha Incorreta'
                            });
                            break;
                        default:
                            $scope.alerts.push({
                                type: 'alert',
                                msg: 'Não foi possivel conectar no servidor'
                            });
                    }
                }else {
                    $scope.loading = false;
                }
            });
        };



  $scope.months = [
 "Janeiro",
  "Fevereiro",
  "Março",
  "Abril",
  "Maio",
  "Junho",
  "Julho",
  "Agosto",
  "Setembro",
  "Outubro",
   "Novembro",
   "Dezembro"
  ];

  $scope.years = [
    2013,
    2014,
    2015,
    2016,
    2017,
    2018,
    2019,
    2020,
    2021,
    2022,
    2023,
    2024
  ];

  $scope.sell = sell;

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.buyTickets = function(e,sellTicket){
        e.preventDefault();
        var isEmpty;

        for (var i in sellTicket) {
            if(sellTicket.hasOwnProperty(i)) {
                isEmpty = false;
                break;
            }
        }
        if(isEmpty == undefined){

            $scope.alerts.push({type: 'warning', msg: 'Formulário não pode ser vazio' });
        }else {
            $scope.sellTicket = sellTicket;
            if($scope.sellTicket.cardName == null){
                $scope.alerts.push({type: 'danger', msg: 'Nome do Cartão é Obrigatório' });
            }
            if($scope.sellTicket.cardNumber == null){
                $scope.alerts.push({type: 'danger', msg: 'Número do Cartão é Obrigatório' });
            }

            if($scope.sellTicket.expiryMonth == null  ||$scope.sellTicket.expiryYear == null ){
                $scope.alerts.push({type: 'danger', msg: 'Data de Vencimento é Obrigatória' });
            }

            if($scope.sellTicket.cardSecurity == null){
                $scope.alerts.push({type: 'danger', msg: 'Número de Segurança é Obrigatório' });
            }

            if($scope.sellTicket.cardType == null){
                $scope.alerts.push({type: 'danger', msg: 'Por favor escolher Bandeira do cartão' });
            }
        }
        if($scope.alerts[0].msg == ""){
            $scope.sellTicket.idTicket = $scope.sell.ticket[0].id_ticket;
        }
        console.log(sellTicket);
        console.log($scope.alerts[0].msg +" " + isEmpty);


    };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});